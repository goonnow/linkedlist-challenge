require 'stack'

RSpec.describe Stack do
  it "#push" do
    st = Stack.new

    st.push(4)
    expect( st.data ).to match_array( [4] )

    st.push(6)
    expect( st.data ).to match_array( [4,6] )
  end

  it "#pop" do
    st = Stack.new
    st.push(3)
    st.push(7)

    item = st.pop()
    expect( item ).to eq 7
    expect( st.data ).to match_array( [3] )

    item = st.pop()
    expect( item ).to eq 3
    expect( st.data ).to match_array( [] )

    item = st.pop()
    expect( item ).to eq nil
    expect( st.data ).to match_array( [] )
  end
end
