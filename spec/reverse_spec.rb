require 'ReverseList'
require 'LinkedListNode'

RSpec.describe ReverseList do
  it "stack reverse" do
    node1 = LinkedListNode.new(37)
    node2 = LinkedListNode.new(99, node1)
    node3 = LinkedListNode.new(12, node2)

    arr = linked_list_2_array( node3 )

    revlist = ReverseList.reverse( node3 )
    rev_arr = linked_list_2_array( revlist )
    expect( arr.reverse ).to match_array( rev_arr )
  end

  it "mutation reverse" do
    node1 = LinkedListNode.new(37)
    node2 = LinkedListNode.new(99, node1)
    node3 = LinkedListNode.new(12, node2)

    arr = linked_list_2_array( node3 )

    revlist = ReverseList.mutation_reverse( node3 )
    rev_arr = linked_list_2_array( revlist )
    expect( arr.reverse ).to match_array( rev_arr )
  end
end


def linked_list_2_array( list )
  arr = Array.new

  while list
      arr.push( list.value )
      list = list.next_node

  end

  return arr
end
