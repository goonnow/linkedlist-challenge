# LinkedList Challenge

## Overview
- `./lib/ReverseList.rb` contains reverse algorithms.
- `./lib/Stack.rb` is an implementation of Stack.


### Testing
```
# Checking output
$ ruby code.rb

# Checking via "rspec"
$ rspec
```