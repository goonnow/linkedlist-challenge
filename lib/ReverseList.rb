require './lib/Stack'
require './lib/LinkedListNode'

class ReverseList

  def self.reverse(list)
      st = Stack.new

      while list
          st.push( list.value )
          list = list.next_node
      end

      first_node = nil
      prev_node = nil
      while value = st.pop
          current_node = LinkedListNode.new( value );

          if prev_node.nil?
            first_node = current_node
          else
            prev_node.next_node = current_node
          end
          prev_node = current_node
      end

      return first_node
  end

  def self.mutation_reverse(list, previous=nil)
    if list.next_node.nil?
      list.next_node = previous
      return list
    else
      next_node = list.next_node
      list.next_node = previous
      return mutation_reverse( next_node, list );
    end
  end
end
