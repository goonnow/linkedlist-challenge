class Stack
    attr_reader :data

    def initialize
        @data = nil
    end

    def push(element)
      if @data == nil
        @data = Array.new
      end
      @data << element
    end

    def pop
        index = @data.size - 1
        res   = @data[index]
        @data.delete_at(index)
        return res
    end

end
