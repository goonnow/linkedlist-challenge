require './lib/LinkedListNode'
require './lib/Stack'
require './lib/ReverseList'

def print_values(list_node)
  print "#{list_node.value} --> "
  if list_node.next_node.nil?
    print "nil\n"
    return
  else
    print_values(list_node.next_node)
  end
end



node1 = LinkedListNode.new(37)
node2 = LinkedListNode.new(99, node1)
node3 = LinkedListNode.new(12, node2)

print_values(node3)

puts "-------"
revlist = ReverseList.reverse(node3)
print_values(revlist)

puts "-------"
mutation_revlist = ReverseList.mutation_reverse(node3)
print_values(mutation_revlist)
